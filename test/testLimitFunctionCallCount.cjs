const limitFunctionCallCount = require("../limitFunctionCallCount.cjs")

function sayName() {
    return "My name is Abhijeet";
}

const limitFnCall = limitFunctionCallCount();

for (let index = 0; index < 6; index++) {
    console.log(limitFnCall(sayName, 4));
}
