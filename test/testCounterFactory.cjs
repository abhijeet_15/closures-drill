const { counterFactory } = require("../counterFactory.cjs");

const counter = counterFactory();

for (let index = 0; index < 5; index++) {
    console.log(counter.increment());
}

for (let index = 0; index < 5; index++) {
    console.log(counter.decrement());
}


