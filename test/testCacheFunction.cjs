const cacheFunction = require("../cacheFunction.cjs");

function square(num) {
    return num * num;
}

const cachedSquare = cacheFunction(square);

console.log(cachedSquare(4));
console.log(cachedSquare(4));