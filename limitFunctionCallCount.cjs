function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    let functionCallCount = 0;

    return function (cb, n) {
        if (functionCallCount < n) {
            functionCallCount++;
            return cb()
        }
        else {
            return null;
        }
    }
}

module.exports = limitFunctionCallCount;